import json

import db

def main_handler(event, context):
    period = receive_from_event_bridge(event)
    response = db.find_all_and_transform_to_dataframe(period)

    return {
        "statusCode": 200,
        "body": json.dumps({
            "Status": response
        })
    }

def receive_from_event_bridge(event):
    print("Receiving event")
    date = event["time"]
    return date[:7]