import os
import psycopg2
import pandas
import boto3
from datetime import date

SCHEMA_NAME = 'employees'
TB_NAME = 'tb_work_hours'

def connect_to_rds():
    try:
        conn = psycopg2.connect(
            host=os.environ['DB_HOST'],
            database=os.environ['DB_NAME'],
            user=os.environ['DB_USER'],
            password=os.environ['DB_PASS'])

        return conn
    except (psycopg2.OperationalError, KeyError) as err:
        print("Error on: connect_to_rds")
        print("Some error happened: ", err)

def find_all_and_transform_to_dataframe(period):
    try:
        conn = connect_to_rds()
        query = f"SELECT * FROM {SCHEMA_NAME}.{TB_NAME} WHERE period = '{period}'"

        reports = pandas.read_sql_query(query,con=conn)

        conn.close()

        dt = pandas.DataFrame(reports)
        transform_to_csv(dt)

        print("Data saved to S3")

        return "Report saved!"
    except Exception as err:
        print("Error on: find_all_and_transform_to_dataframe")
        print("Some error happened: ", err)

def transform_to_csv(dt):
    try:
        print("Transforming to CSV")
        file_name = date.today()
        dt.to_csv(f'/tmp/{file_name}.csv')
        upload_to_s3(file_name)
    except Exception as err:
        print("Error on: transform_to_csv")
        print("Some error happened: ", err)

def upload_to_s3(file_name):
    try:
        print("Uploading to S3")
        s3 = boto3.resource('s3')
        bucket = "employees-work-hours"
        s3.Bucket(bucket).upload_file(f"/tmp/{file_name}.csv", f"reports/{file_name}.csv")
    except Exception as err:
        print("Error on: upload_to_s3")
        print("Some error happened: ", err)