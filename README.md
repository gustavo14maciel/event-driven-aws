![AWS Architecture using different services](./images/Event-driven%20Python%20Lambda.png)

# Arquitetura Orientada a Eventos 

## Serviços AWS

- API Gateway: Controla a comunicação do front-end com o Lambda dentro da AWS

- Lambda: Roda código de forma serveless em resposta a eventos

- SQS: Serviço de mensageria/fila

- SQS DLQ: Fila de mensagens que não foram entregues com sucesso

- RDS: Serviço de Banco de Dados relacional gerenciado pela AWS

- EventBridge: Envia eventos para o Lambda em um schedule

- S3: Armazenamento dos CSVs gerados pelo Lambda

- SNS: Gera notificação via e-mail quando um objeto é adicionado ao bucket S3

### Lambdas

- Authorizer: Verifica o token do front-end e, se for válido, passa para o API Gateway a permissão de enviar o request para o próximo Lambda 

- UI to SQS: Envia os dados do report do funcionário para a fila do SQS

- SQS to RDS: Quando uma mensagem é colocada na fila, o SQS envia um evento para o Lambda consumir essa mensagem com os dados do report e criar o report das horas trabalhadas no mês do funcionário em determinado período (ou atualizar se já existir um report do período) no RDS

- Generate CSV and Notify: O EventBridge envia um evento uma vez por mês para o Lambda gerar um CSV com os reports de todos os funcionários que criaram um report para o mês em específico e armazena no S3
