import json
import boto3
from botocore.exceptions import ClientError

def main_handler(event, context):
    return send_message(event)

def send_message(event):
    try:
        sqs = boto3.resource('sqs', region_name="sa-east-1")
        queue = sqs.get_queue_by_name(QueueName='LambdaQueue')
        print("Sending message to SQS")
        response = queue.send_message(MessageBody=json.dumps(event["data"]))
        print("Message sent:", response)
        return {
            "statusCode": 200,
            "body": json.dumps({
                "Status": "Message delivered"
            })
        }
    except ClientError as err:
        return {
            "statusCode": 400,
            "body": json.dumps({
                "Error": err
            })
        }
