import os

def main_handler(event, context):
    print("Event: ", event)

    # Check if token is valid
    auth = "Deny"
    if event["authorizationToken"] == os.environ['AUTH_TOKEN']:
        auth = "Allow"
    else:
        auth = "Deny"

    auth_response = {
        "principalId": "1",
        "policyDocument":
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Action": "execute-api:Invoke",
                        "Resource": [
                            f"arn:aws:execute-api:{os.environ['AWS_REGION']}:{os.environ['AWS_ACCOUNT_NUMBER']}:{os.environ['AWS_API_GATEWAY_ID']}/*/*"
                        ],
                        "Effect": auth
                    }
                ]
            }
    }
    return auth_response

