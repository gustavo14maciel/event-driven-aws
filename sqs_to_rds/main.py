import json
import db

def main_handler(event, context):
    employee = receive_from_sqs(event)
    dict_employee = json.loads(employee)
    response = db.process_hours(dict_employee)

    return {
        "body": json.dumps({
            "Status": response
        })
    }

def receive_from_sqs(event):
    print("Receiving event")
    data = event["Records"][0]["body"]

    return data

