import os

import psycopg2

SCHEMA_NAME = 'employees'
TB_NAME = 'tb_work_hours'

def connect_to_rds():
    try:
        conn = psycopg2.connect(
            host=os.environ['DB_HOST'],
            database=os.environ['DB_NAME'],
            user=os.environ['DB_USER'],
            password=os.environ['DB_PASS'])

        cursor = conn.cursor()

        return cursor, conn
    except (psycopg2.OperationalError, KeyError) as err:
        print("Some error happened: ", err)

def find_by_email_and_period(email, period, cursor):
    try:
        query = f"SELECT * FROM {SCHEMA_NAME}.{TB_NAME} WHERE email = '{email}' and period = '{period}'"

        cursor.execute(query)

        if cursor.fetchone() is not None:
            return "Update"
        else:
            return "Create"
    except psycopg2.OperationalError as err:
        print("Some error happened: ", err)

def process_hours(employee):
    cursor, conn = connect_to_rds()

    update_or_create = find_by_email_and_period(employee.get("email"), employee.get("date"), cursor)

    if update_or_create == "Create":
        return create_report(employee, cursor, conn)
    elif update_or_create == "Update":
        return update_report(employee, cursor, conn)

def create_report(employee, cursor, conn):
    try:
        print("Creating employee report")
        client_codes = format_client_codes(employee.get('client_codes'))

        values = f"{employee.get('employee'), employee.get('manager'), employee.get('email'), employee.get('total_hours'), employee.get('date'), client_codes, employee.get('location'), employee.get('contract_type')}"
        query = f'INSERT INTO {SCHEMA_NAME}.{TB_NAME} (employee, manager, email, total_hours, "period", client_codes, "location", contract_type) VALUES {values}'

        cursor.execute(query)
        conn.commit()

        print("Row created")

        cursor.close()

        return "Employee report created!"
    except (Exception, psycopg2.DatabaseError) as err:
        print("Some error happened: ", err)

        return err

def update_report(employee, cursor, conn):
    try:
        print("Updating employee row")
        client_codes = format_client_codes(employee.get('client_codes'))
        query = f"UPDATE {SCHEMA_NAME}.{TB_NAME} SET manager = '{employee.get('manager')}', total_hours = {employee.get('total_hours')}, client_codes = '{client_codes}', location = '{employee.get('location')}', contract_type = '{employee.get('contract_type')}' WHERE email = '{employee.get('email')}';"

        cursor.execute(query)
        conn.commit()

        print("Row updated")

        cursor.close()

        return "Employee report updated!"
    except (Exception, psycopg2.DatabaseError) as err:
        print("Some error happened: ", err)

        return err

def format_client_codes(client_codes):
    return ','.join(client_codes)